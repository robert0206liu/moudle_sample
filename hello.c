/*
 * =====================================================================================
 *
 *       Filename:  hello.c
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  2015年04月21日 10時18分57秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  YOUR NAME (), 
 *   Organization:  
 *
 * =====================================================================================
 */
#include <linux/kernel.h> /*   pr_info 所需 include 檔案*/
#include <linux/init.h>
#include <linux/module.h> /*   所有 module 需要檔案*/
#include <linux/version.h>
#include <linux/sched.h>

MODULE_DESCRIPTION("Hello World !!");
MODULE_AUTHOR("Robert Liu");
MODULE_LICENSE("GPL");

static int __init hello_init(void)
{
        pr_info("Hello, world\n");
        pr_info("The process is \"%s\" (pid %i)\n", current->comm, current->pid);
        return 0;
}

static void __exit hello_exit(void)
{
        printk(KERN_INFO "Goodbye\n");
}

module_init(hello_init);
module_exit(hello_exit);

